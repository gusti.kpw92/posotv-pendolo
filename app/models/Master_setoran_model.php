<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_setoran_model extends CI_Model {

  var $table = 'master_setoran';
  var $vtable = 'v_master_setoran';
  var $column = array('id_master_setoran','tgl_setoran','kolektor',' acc','keterangan');
  var $order = array(
    'tgl_setoran' => 'DESC'
  );

  function __construct()
  {
      parent::__construct();
  }

  /*
    Serverside datatable
  */

  private function _get_datatables_query()
  {

    $this->db->from($this->vtable);
    $i = 0;
    foreach ($this->column as $item) // loop column
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $column[$i] = $item; // set column array variable to order processing
      $i++;
    }

    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->vtable);
    return $this->db->count_all_results();
  }

  /*
    CRUD Master Setoran
  */

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_master_setoran',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db->insert('master_setoran', $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update('master_setoran', $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by_id($id)
  {
    $this->db->where('id_master_setoran', $id);
    $this->db->delete('master_setoran');
  }


  /*
  public function update($where, $data)
  {
    $this->db->update('settings', $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by($blnPenagihan)
  {
    $this->db->like('bulan_penagihan', $blnPenagihan, 'after');
    $this->db->delete('temp_invoice');
  }

  public function cekInvoiceCode($cari) {
    $this->db->select_max('invoice','last');  // artinya SELECT max(kode_plgn) as last
    $this->db->from('temp_kwitansi');
    $this->db->like('invoice',$cari,'after'); // artinya WHERE kode_plgn LIKE '$wilayah%'
                                                      // after = $wilayah%    <- lihat posisi persennya(%)
                                                      // before = %$wilayah
                                                      // both = %$wilayah%
    $cek = $this->db->get();
    return $cek->row();
  }

  public function lastPayed($kode_plgn)
  {
    $this->db->select_max('bulan_bayar','lastpayed');
    $this->db->from('pembayaran');
    $this->db->where('kode_pelanggan',$kode_plgn);
    $cek = $this->db->get();
    return $cek->row();

  }

  public function cekIDWil($kode_wilayah)
  {
    $query = $this->db->query("SELECT * FROM wilayah WHERE kode_wilayah='$kode_wilayah'");
    return $query->row();
  }

  public function count_pel($id_wilayah)
  {
    $query = $this->db->query(" SELECT count(p.kode_pelanggan) AS jumlah FROM pelanggan p WHERE p.wilayah = '$id_wilayah'");
    return $query->row();
  }

  public function save_inv($data)	{
		$this->db->insert('temp_invoice', $data);
		return $this->db->insert_id();
	}

  public function findCollector($wilayah)
  {
    return $query = $this->db->query("SELECT * FROM v_kolektor WHERE wilayah LIKE '%$wilayah%'")->row();
  }

  public function cek_temp_invoice($kode_pelanggan,$bulanPenagihan)
  {
    return $query = $this->db->query("SELECT * FROM v_temp_invoice WHERE kode_pelanggan LIKE '%$kode_pelanggan%' AND bulan_penagihan LIKE '%$bulanPenagihan%' ")->row();
  }

  public function getDetailTagihan($scanedQR)
  {
    return $query = $this->db->query("SELECT * FROM v_temp_invoice WHERE hash='$scanedQR' OR kode_invoice='$scanedQR'");
  }

  public function updateSetoran($where, $data)
  {
    $this->db->update('temp_invoice', $data, $where);
    return $this->db->affected_rows();
  }

  */

}
