<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup_db extends CI_Controller {

	function __construct()
	{
			parent::__construct();
			if (!is_logged_in()) {
				redirect('login');
			}
			$this->load->dbutil();
	}

	public function index()
	{
		set_status_header(401);
	}

	public function backup()
	{
		$filename = "backup_".date('Y-m-d H_m_s').".sql";
		$prefs = array(
        'tables'        => array('profil_perusahaan',
					'settings',
					'bagian',
					'jabatan',
					'jenis_gangguan',
					'tarif',
					'status',
					'wilayah',
					'karyawan',
					'kolektor',
					'pelanggan',
					'pengaduan',
					'tagihan',
					'temp_invoice',
					'users',
					'master_setoran',
					'detail_setoran',
					'profil_perusahaan',
					'statistik_bulanan',
					'surat',
					'v_karyawan',
					'v_kolektor',
					'v_pelanggan',
					'v_pengaduan',
					'v_temp_invoice',
					'v_detail_setoran',
					'v_display_pengaduan',
					'v_gis_pelanggan',
					'v_master_setoran',
					'v_setoran_bulan_ini',
					'v_tunggakan',
					'v_users'),   // Array of tables to backup.
        'ignore'        => array(),                     // List of tables to omit from the backup
        'format'        => 'txt',                       // gzip, zip, txt
        'filename'      => $filename,              // File name - NEEDED ONLY WITH ZIP FILES
        'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
        'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
        'newline'       => "\n"                         // Newline character used in backup file
		);

		$backup = $this->dbutil->backup($prefs);
		write_file(FCPATH.'assets/backup/'.$filename, $backup);

		// $this->load->helper('download');
		// force_download($filename, $backup);

		echo "Success to creating backup! Check : ".base_url().'assets/backup/'.$filename;
		// echo FCPATH.'/assets/backup/'.$filename;
	}

}
