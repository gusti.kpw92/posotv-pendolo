<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Master_setoran extends CI_Controller {

	function __construct()
  {
      parent::__construct();
			if (!is_logged_in()) {
				redirect('login');
			}
      $this->load->model('master_setoran_model','setoran');
  }

	public function index()
	{
		set_status_header(401);
	}

	/*
		Serverside DataTable
	*/

	public function ajax_list() {
	$list = $this->setoran->get_datatables();
	$data = array();
	$no = $_POST['start'];
	foreach ($list as $br) {
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "<span class='font-bold'>$br->tgl_setoran</span>";
		$row[] = "<span class='font-bold'>$br->kolektor</span>";
		$row[] = ($br->acc == 'N') ? 'NO' : 'YES';
		$row[] = "Rp ".number_format($br->total_setoran,0,",",".");
		$row[] = $br->keterangan;
		$row[] = "<a class=\"btn btn-xs btn-primary\" href=\"".site_url('pindai/').$br->id_master_setoran."\" title=\"Scan Kwitansi\"><i class=\"fa fa-qrcode\"></i></a>
							<a class=\"btn btn-xs btn-warning\" href=\"javascript:void(0)\" onclick=\"edits('$br->id_master_setoran')\" title=\"Edit\"><i class=\"glyphicon glyphicon-pencil\"></i></a>
							<a class=\"btn btn-xs btn-danger\" href=\"javascript:void(0)\" onclick=\"deletes('$br->id_master_setoran')\" title=\"Hapus\" ><i class=\"glyphicon glyphicon-trash\"></i></a>";
		$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setoran->count_all(),
						"recordsFiltered" => $this->setoran->count_filtered(),
						"data" => $data,
				);
		echo json_encode($output);
	}

	/*
		CRUD Master Setoran
	*/

	public function save()
	{
		$this->_validate();
		$data = array(
			'tgl_setoran' => $this->input->post('tgl_setoran'),
			'id_kolektor' => $this->input->post('id_kolektor'),
			'acc' => ($this->input->post('acc') == null) ? 'N' : '',
			'keterangan' => $this->input->post('keterangan'),
		);
		$insert = $this->setoran->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update()
	{
		$this->_validate();
		$data = array(
			'tgl_setoran' => $this->input->post('tgl_setoran'),
			'id_kolektor' => $this->input->post('id_kolektor'),
			// 'acc' => ($this->input->post('acc') == null) ? 'N' : '',
			'keterangan' => $this->input->post('keterangan'),
		);
		$this->setoran->update(array('id_master_setoran' => $this->input->post('id_master_setoran')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function delete($id)
	{
		$this->setoran->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_edit($id=FALSE)
	{
		$data= $this->setoran->get_by_id($id);
		echo json_encode($data);
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		//id tidak divalidasi karena auto_increment
		if($this->input->post('tgl_setoran') == '') {
			$data['inputerror'][] = 'tgl_setoran';
			$data['error_string'][] = 'Enter this field!';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


/*

	public function getDetailTagihan($scanedQR="")
	{
		if ($scanedQR !="") {
			$query = $this->setoran->getDetailTagihan($scanedQR);
			if ($query->num_rows() !== 0) {
				$q = $query->row();
				$status = ($q->status == 'Lunas') ? "<i class='fa fa-check fa-2x text-success' data-toggle='tooltip' title='$q->status'></i>" : "<i class='fa fa-clock-o fa-2x' data-toggle='tooltip' title='$q->status'></i>" ;
				$row = "<tr class='$q->kode_invoice'>
				<td><input name='kode_invoice[]' value='$q->kode_invoice' hidden>$q->kode_invoice</td>
				<td>$q->kode_pelanggan</td>
				<td>$q->nama_lengkap</td>
				<td>$q->wilayah</td>
				<td>".str_replace('-02','',$q->bulan_penagihan)."</td>
				<td>$status <input name='status[]' value='$q->status' hidden></td>
				<td>$q->tarif <input type='number' id='$q->kode_invoice' value='$q->tarif' hidden></td>
				<td><input type='number' name='jmlsetoran[]' class='form-control input-sm' value='$q->tarif'></td>
				<td class='text-center'>
					<a href='javascript:void(0)' class='btn btn-xs btn-info' onclick=\"addKet('$q->kode_invoice')\"><i class='fa fa-info'></i> Add Info</a>
					<a href='javascript:void(0)' class='btn btn-xs btn-danger' onclick=\"hapusTr('$q->kode_invoice')\"><i class='fa fa-trash'></i> Hapus</a>
					<textarea name='keterangan[]' class='form-control input-sm $q->kode_invoice' placeholder='Tambahkan keterangan setoran ini' style='display:none'>$q->keterangan</textarea>
					<input name='hash[]' value='$q->kode_invoice' hidden>
				</td>
				</tr>";
				$hash = $q->kode_invoice;
			} else {
				$row = $hash = "";
			}
		} else {
			$row = $hash = "";
		}

		$data = array('data' => $row,
							'hash' => $hash,
							'tarif' =>(int) ($q->tarif) ? $q->tarif : 0,
		);
		echo json_encode($data);
	}

	public function setor()
	{
		$ceksession = base64_decode(urldecode($this->session->sesikode));

		$nama_kolektor = $this->input->post('nama_kolektor');
		$kode_invoice = $this->input->post('kode_invoice[]');
		$jmlsetoran = $this->input->post('jmlsetoran[]');
		$status = $this->input->post('status[]');
		$ket = $this->input->post('keterangan[]');

		for ($i=0; $i < count($hash); $i++) {
			// Get user berdasarkan wilayah kolektor
			$inv = $kode_invoice[$i];
			$cek_wil = $this->db->query("SELECT * FROM v_temp_invoice WHERE kode_invoice LIKE '$inv'")->row();
			$cek_kolektor = $this->db->query("SELECT * FROM v_kolektor WHERE wilayah LIKE '%$cek_wil->id_wilayah%'")->row();
			$sesi = ($ceksession == '1') ? $cek_kolektor->id_kolektor : $cek_kolektor->id_kolektor ;

			$data = array(
				// 'user' => $sesi,
				'status' => 'Lunas',
				'tgl_bayar' => date('Y-m-d'),
				'keterangan' => $ket[$i],
			);
			if ($status[$i] != 'Lunas') { // lakukan input data hanya jika invoice berstatus belum lunas
				$this->setoran->updatesetoran(array('kode_invoice' => $kode_invoice[$i], 'hash' => $hash[$i], ), $data);
			}
		}

		echo json_encode(array('status' => TRUE, ));
	}

	public function tes2()
	{
		$this->load->view('admin/setoran/qrscan');
	}

*/

}
