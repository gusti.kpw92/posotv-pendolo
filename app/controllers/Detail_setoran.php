<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Detail_setoran extends CI_Controller {

	function __construct()
  {
      parent::__construct();
			if (!is_logged_in()) {
				redirect('login');
			}
			$this->load->model('detail_setoran_model','setoran');
      $this->load->model('profil_perusahaan_model','dsh');
  }

	public function index()
	{
		set_status_header(401);
	}

	public function detail($id_master_setoran = FALSE)
	{
		$data['profilP'] = $this->dsh->get_by_id(1);
		$data['detail_setoran'] = $this->setoran->getDetailKolektor($id_master_setoran);
		$data['active'] = "detail_setoran";
		$this->load->view("admin/templates/sheader",$data);
		$this->load->view("admin/templates/aside");
		$this->load->view("admin/detail_setoran/detail_setoran");
		$this->load->view("admin/templates/footer");
		$this->load->view("admin/templates/sfooter");
		$this->load->view("admin/detail_setoran/js_detail_setoran");
		// echo json_encode($data);
	}

	/*
		CRUD detail Setoran
	*/
	public function checkInvoice($id, $kodeInvoice, $id_kolektor)
	{
		// cek data di temp_invoice & detail_setoran
		$cek = $this->setoran->cekInvoiceCode($id, $kodeInvoice, $id_kolektor);
		if ($cek == 3) {
			$data = array(
				'status' => TRUE,
				'message' => 'Duplikat input! Data sudah ada dalam setoran sebelumnya.',
				'title' => 'Sudah diinput!',
				'code' => 3,
			);
		} else if ($cek == 0) {
			$data = array(
				'status' => TRUE,
				'message' => 'INVOICE belum terdaftar dalam database!',
				'title' => 'Unknown Invoice!',
				'code' => 0,
			);
		} else if ($cek == 4) {
			$simpan = $this->_save_detail_setoran($id,$kodeInvoice);
			$data = array(
				'status' => TRUE,
				'message' => 'Invoice manifested!',
				'title' => 'Sukses!',
				'code' => 4,
			);
		} else if ($cek == 2) {
			$data = array(
				'status' => TRUE,
				'message' => 'Invoice & Kolektor tidak sesuai atau Wilayah INVOICE tidak sesuai wilayah KOLEKTOR!',
				'title' => 'Salah input!',
				'code' => 2,
			);
		}

		echo json_encode($data);
	}

	private function _save_detail_setoran($id,$kodeInvoice)
	{
		$dataTemp = $this->setoran->getDataInvoiceBy($kodeInvoice);
		if ($dataTemp->num_rows() == 1) {
			$dt = $dataTemp->row();
			$data = array(
				'id_master_setoran' => $id,
				'kode_invoice' => $dt->kode_invoice,
				'kode_pelanggan' => $dt->kode_pelanggan,
				'bulan_penagihan' => $dt->bulan_penagihan,
				'status' => 'Lunas',
				'tgl_bayar' => date('Y-m-d'),
			);

			$dataUpdate = array(
				'status' => 'Lunas',
				'tgl_bayar' => date('Y-m-d'),
			);

			$this->update_status_temp_invoice($kodeInvoice, $dataUpdate);
			$insert = $this->setoran->save($data);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_status_temp_invoice($kodeInvoice, $dataUpdate)
	{
		$update = $this->setoran->updateStatusTemp(array('kode_invoice' => $kodeInvoice), $dataUpdate);
	}

	// public function delete($id)
	// {
	// 	$this->setoran->delete_by_id($id);
	// 	echo json_encode(array("status" => TRUE));
	// }

	public function get_edit($id=FALSE)
	{
		$data= $this->setoran->get_by_id($id);
		echo json_encode($data);
	}

	public function list_setoran($id_master_setoran)
	{
		$list = $this->setoran->listSetoranBy($id_master_setoran);
		$this->_updateTotalSetoran($id_master_setoran);

		$row = '';
		if ($list->num_rows() > 0) {
			foreach ($list->result() as $d) {
				$status = ($d->status == 'Lunas') ? "<i class='fa fa-check fa-2x text-success' data-toggle='tooltip' title='$d->status'></i>" : "<i class='fa fa-clock-o fa-2x' data-toggle='tooltip' title='$d->status'></i>" ;
				$row .= "<tr class='success' title='Keterangan : $d->keterangan'>
				<td><div class=\"i-checks\"><label> <input type=\"checkbox\" name=\"checked_inv[]\" value=\"$d->kode_invoice\"> <i></i> $d->kode_invoice </label></div></td>
				<td>$d->kode_pelanggan</td>
				<td>$d->nama_lengkap</td>
				<td>$d->wilayah</td>
				<td>".str_replace('-02','',$d->bulan_penagihan)."</td>
				<td>$status</td>
				<td>".number_format($d->tarif,0,",",".")."</td>
				<td class='text-center'>
					<a href='javascript:void(0)' class='btn btn-xs btn-info' onclick=\"getKeterangan('$d->kode_pelanggan','$d->kode_invoice')\" title=\"Tambah keterangan?\"><i class='fa fa-info'></i> Ket</a>
					<a href='javascript:void(0)' class='btn btn-xs btn-danger' onclick=\"delete_by('$d->kode_invoice')\" title=\"Hapus hasil scan?\"><i class='fa fa-trash'></i> Hapus</a>
				</td>
				</tr>";
			}
		} else {
			$row .= '<tr class="danger">
				<td colspan="9" class="text-center text-danger"><h3>Tidak ada data! Silahkan Scan Robekan Kwitansi pada Kamera!</h3></td>
			</tr>';
		}

		$data = array(
			'data' => $row,
		);

		echo json_encode($data);
	}

	public function invoiceCountBy($id_master_setoran)
	{
		$q = $this->setoran->getInvoiceCountBy($id_master_setoran);

		$row = ''; $no = 0; $totaljml = 0; $totalrp = 0;
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $d) {
				$no++;
				$row .= "
				<tr>
					<td>$no</td>
					<td class='text-right'>".number_format($d->tarif,0,",",".")."</td>
					<td class='text-right'>$d->jumlah</td>
					<td class='text-right'>".number_format($d->subtotal,0,",",".")."</td>
				</tr>";
				$totaljml += $d->jumlah;
				$totalrp += $d->subtotal;
			}
		} else {
			$row .= '<tr class="noData">
				<td colspan="4" class="text-center text-danger"><h3>Tidak ada data!</h3></td>
			</tr>';
		}

		$row .= "<tr><td colspan='2' class='info'>Total Setoran</td><td class='text-right'><strong>$totaljml</strong></td><td class='text-right'><strong>Rp ".number_format($totalrp,0,",",".")."</strong></td></tr>";
		$row .= "<tr><td colspan='2' class='warning'>Total Komisi</td><td class='text-right'><strong>x 3.000</strong></td><td class='text-right'><strong>Rp ".number_format($totaljml*3000,0,",",".")."</strong></td></tr>";
		$row .= "<tr><td colspan='2' class='success'>Setoran Masuk</td><td class='text-right'><strong></strong></td><td class='text-right'><strong>Rp ".number_format($totalrp - ($totaljml*3000),0,",",".")."</strong></td></tr>";

		$data = array(
			'data' => $row,
			'total_kwitansi' => $totaljml,
			'total_rp' => $totalrp,
		);

		echo json_encode($data);
	}

	public function get_keterangan($kode_pelanggan)
	{
		$q = $this->setoran->getKeteranganPlgn($kode_pelanggan);
		$data = array('keterangan' => $q->keterangan, 'kode_pelanggan' => $q->kode_pelanggan, );
		echo json_encode($data);
	}

	public function save_keterangan()
	{
		$data = array(
		'keterangan' => html_escape($this->input->post('md_keterangan')),
		);

		$this->setoran->updateKeteranganPlgn(array('kode_pelanggan' => html_escape($this->input->post('md_kode_pelanggan'))), $data);
		$this->setoran->updateKeterangan(array('kode_invoice' => html_escape($this->input->post('md_kode_invoice'))), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function delete_by($kode_invoice)
	{
		$dataUpdate = array(
			'status' => 'Belum Bayar',
			'tgl_bayar' => '0000-00-00',
		);

		$this->update_status_temp_invoice($kode_invoice, $dataUpdate);
		$this->setoran->deleteBy($kode_invoice);
		echo json_encode(array("status" => TRUE));
	}

	public function delAllBy($id_master_setoran)
	{
		$this->setoran->deleteAllBy($id_master_setoran);
		echo json_encode(array("status" => TRUE));
	}

	private function _updateTotalSetoran($id_master_setoran)
	{
		$q = $this->setoran->hitungSetoranBy($id_master_setoran);
		$data = array(
			'total_setoran' => $q->total_setoran,
			'lembar' => $q->lembar,
			'komisi' => $q->komisi,
		);
		$this->setoran->updateMasterSetoran(array('id_master_setoran' => $id_master_setoran), $data);
		return json_encode(array("status" => TRUE));
	}

	public function tesdatax($id_master_setoran)
	{
		$list = $this->setoran->listSetoranBy($id_master_setoran);
		$this->_updateTotalSetoran($id_master_setoran);
		$no = 1;
		$dt = array();
		if ($list->num_rows() > 0) {
			foreach ($list->result() as $d) {
				$row = array();
				$status = ($d->status == 'Lunas') ? "<i class='fa fa-check fa-2x text-success' data-toggle='tooltip' title='$d->status'></i>" : "<i class='fa fa-clock-o fa-2x' data-toggle='tooltip' title='$d->status'></i>" ;
				// $row[] = "<input type=\"checkbox\" name=\"checked_inv[]\" value=\"$d->kode_invoice\" class=\"i-checks\">";
				$row[] = $no;
				$row[] = "<b>$d->kode_invoice</b>";
				$row[] = $d->kode_pelanggan;
				$row[] = $d->nama_lengkap;
				$row[] = $d->wilayah;
				$row[] = str_replace('-02','',$d->bulan_penagihan);
				$row[] = $status;
				$row[] = number_format($d->tarif,0,",",".");
				$row[] = "<a href='javascript:void(0)' class='btn btn-xs btn-info' onclick=\"getKeterangan('$d->kode_pelanggan','$d->kode_invoice')\" title=\"Tambah keterangan?\"><i class='fa fa-info'></i> Ket</a>
								<a href='javascript:void(0)' class='btn btn-xs btn-danger' onclick=\"delete_by('$d->kode_invoice')\" title=\"Hapus hasil scan?\"><i class='fa fa-trash'></i> Hapus</a>";
				$dt[] = $row;
				$no++;
			}
		} else {
			$dt = array(
					"draw" => 0,
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"data" => [],
			);
			// $row[] = '<h3>Tidak ada data! Silahkan Scan Robekan Kwitansi pada Kamera!</h3>';
			// $dt[] = $row;
		}

		$data = array(
			'data' => $dt,
		);

		echo json_encode($data);
	}

	public function get_last_inserted($id_master_setoran,$kode_invoice)
	{
		$qd = $this->setoran->getLastInserted($id_master_setoran,$kode_invoice);
		if ($qd->num_rows() !== 0) {
				$d = $qd->row();
				$count = $this->setoran->getLastNum($id_master_setoran);
				// $count++;
				$status = ($d->status == 'Lunas') ? "<i class='fa fa-check fa-2x text-success' data-toggle='tooltip' title='$d->status'></i>" : "<i class='fa fa-clock-o fa-2x' data-toggle='tooltip' title='$d->status'></i>" ;

				$data = array(
					'data' => array(
						// "<input type=\"checkbox\" name=\"checked_inv[]\" value=\"$d->kode_invoice\" class=\"i-checks\">",
						$count,
						$d->kode_invoice,
						$d->kode_pelanggan,
						$d->nama_lengkap,
						$d->wilayah,
						str_replace('-02','',$d->bulan_penagihan),
						$status,
						number_format($d->tarif,0,",","."),
						"<a href='javascript:void(0)' class='btn btn-xs btn-info' onclick=\"getKeterangan('$d->kode_pelanggan','$d->kode_invoice')\" title=\"Tambah keterangan?\"><i class='fa fa-info'></i> Ket</a>
						<a href='javascript:void(0)' class='btn btn-xs btn-danger' onclick=\"delete_by('$d->kode_invoice')\" title=\"Hapus hasil scan?\"><i class='fa fa-trash'></i> Hapus</a>",
					),
					'status' => TRUE,
				);
				echo json_encode($data);
		}
		else {
			echo json_encode(array('status' => FALSE));
		}
	}


}
