<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-12">
      <h2><?php echo ucwords(str_replace('_',' ',$active)); ?></h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
      <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Laporan Penagihan</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">#</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <p>Silahkan tentukan TAHUN dan WILAYAH yang akan di export dalam bentuk EXCEL!</p>
                                <div class="col-lg-6">
                                  <form id="formLap" action="#" class="form-horizontal">
                                      <div class="col-lg-12 b-r">
                                        <div class="form-group"><label class="col-lg-4 control-label ">Tahun Laporan</label>
                                            <div class="col-lg-8">
                                              <select class="form-control" name="tahun_laporan">
                                                <?php for ($i=2018; $i <= date('Y'); $i++) {
                                                  echo "<option value=\"$i\">$i</option>";
                                                } ?>
                                              </select>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label ">Wilayah</label>
                                            <div class="col-lg-8"><select name="id_wilayah" class="form-control"></select> <span class="help-block m-b-none"></span>
                                            </div>
                                        </div>
                                        <div>
                                          <span class="fa fa-file-excel-o fa-2x"></span> | Support Office Excel 2016
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="button" onclick="export_lap_penagihan()">Export to .Xlsx</button>
                                        </div>
                                      </div>
                                  </form>
                                </div>
                                <div class="col-lg-6">
                                  <!-- <span class="fa fa-file-excel-o fa-2x"></span> -->
                                </div>


                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <p>Coming soon!</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


      </div>
