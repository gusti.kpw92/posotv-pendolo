<script src="<?php echo base_url('assets/inspinia271/js/plugins/dataTables/datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/inspinia271/js/plugins/iCheck/icheck.min.js') ?>"></script>


<script>
var dataScan, tabelku, kode_invoice;
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    listScanned($("#id_master_setoran").val());
  });

// InstaScan QRCode
  let scanner = new Instascan.Scanner({
    video: document.getElementById('preview'),
    scanPeriod: 10
  });
  scanner.addListener('scan', function (content) {
    getDetail(content,'kamera');
    kode_invoice = content;
  });
  Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 0) {
      var selectedCam = cameras[0];
      scanner.start(selectedCam);
      $("#listCamera").html("<h4 class='font-bold text-success'>"+selectedCam.name+"</h4>");
    } else {
      $("#listCamera").html("<h4 class='font-bold text-danger'>No cameras found</h4>");
      // console.error('No cameras found.');
    }
  }).catch(function (e) {
    console.error(e);
    // alert(e);
  });

  var cekMethod = '';

  function getDetail(kode,cekMethod) {
    var url, kode;
    if (cekMethod == 'inputmanual') {
      kode = $("[name='kode_invoice']").val();
    } else {
      kode = kode;
    }

    url = "<?=site_url('detail_setoran/checkInvoice/') ?>"+ $('#id_master_setoran').val() + '/' + kode + '/' + $("[name='id_kolektor']").val();

    $.ajax({
      url : url,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        if (data.code == 0 || data.code == 2 || data.code == 3) {
          notif(data.message,data.title,'error');
        } else {
          load_inserted($("#id_master_setoran").val(), kode);
          //listScanned($("#id_master_setoran").val());
        }
      },
      error : function(jqXHR,errorThrown,textStatus)
      {
        alert('error getting data from server!'+textStatus);
      }
    });
  }

  function listScanned(id_master_setoran) {
    $.ajax({
      url : "<?=site_url('detail_setoran/tesdatax/')?>"+id_master_setoran,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        setTable(data.data);
        countScanned(id_master_setoran);
      },
      error : function(jqXHR,errorThrown,textStatus)
      {
        alert('error getting data from server!'+textStatus);
      }
    });
  }

  function countScanned(id_master_setoran) {
    $.ajax({
      url : "<?=site_url('detail_setoran/invoiceCountBy/')?>"+id_master_setoran,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        $("#kwitansiCount").html(data.data);
      },
      error : function(jqXHR,errorThrown,textStatus)
      {
        alert('error getting data from server!'+textStatus);
      }
    });
  }

  function getKeterangan(kodePelanggan,kodeInvoice) {
    $.ajax({
      url : "<?=site_url('detail_setoran/get_keterangan/')?>"+kodePelanggan+"/"+kodeInvoice,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        $('[name="md_kode_pelanggan"]').val(data.kode_pelanggan);
        $('[name="md_kode_invoice"]').val(kodeInvoice);
        $('#md_kd_pelanggan').text(data.kode_pelanggan);
        $('[name="md_keterangan"]').val(data.keterangan);
        $('#myModal').modal('show'); // show bootstrap modal
      },
      error : function(jqXHR,errorThrown,textStatus)
      {
        alert('error getting data from server!'+textStatus);
      }
    });
  }

  function saveKeterangan() {
    $.ajax({
      url : "<?=site_url('detail_setoran/save_keterangan') ?>",
      type: "POST",
      data: $('#formKet').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if(data.status)
        {
          $('#myModal').modal('hide');
          // listScanned($("#id_master_setoran").val());
          // setCheckBox();
          notif('Berhasil menambah/edit data!','Sukses','success');
        } else {
          alert('Keterangan gagal di update!');
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        notif('Gagal mengUpdate data!','Error','error');
      }
    });
  }

  function delete_by(kodeInvoice) {
    if (confirm('Hapus setoran "'+kodeInvoice+'" ini?')){
      $.ajax({
        url : "<?=site_url('detail_setoran/delete_by/')?>"+kodeInvoice,
        type : "GET",
        dataType : "JSON",
        success : function(data)
        {
          var filteredData = tabelku.rows().indexes().filter( function ( value, index ) {
                 return tabelku.row(value).data()[1] == kodeInvoice;
              });
          tabelku.rows( filteredData )
          .remove()
          .draw();
          notif('Berhasil menghapus data!','Sukses','success');
          //listScanned($("#id_master_setoran").val());
        },
        error : function(jqXHR,errorThrown,textStatus)
        {
          alert('error getting data from server!'+textStatus);
        }
      });
    }
  }

  function deleteAllBy(id_master_setoran) {
    if (confirm('Hapus semua setoran kolektor ini?')){
      $.ajax({
        url : "<?=site_url('detail_setoran/delAllBy/')?>" + id_master_setoran,
        type : "GET",
        dataType : "JSON",
        success : function(data)
        {
          notif('Berhasil menghapus data!','Sukses','success');
          listScanned($("#id_master_setoran").val());
        },
        error : function(jqXHR,errorThrown,textStatus)
        {
          alert('error getting data from server!'+textStatus);
        }
      });
    }
  }

  function setUpper(){
    var kode = $("[name='kode_invoice']").val().toUpperCase();
    $("[name='kode_invoice']").val(kode);
  }

  function setTable(dataScan) {
    tabelku = $('#table2').DataTable({
      'data': dataScan,
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': true
         }
      ],
      'order': [[0, 'desc']]
    });
    setCheckBox();
  }

  function setCheckBox() {
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
  }

  function load_inserted(id_master_setoran,kode) {
    $.ajax({
      url : "<?=site_url('detail_setoran/get_last_inserted/')?>"+id_master_setoran+"/"+kode,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        if (data.status) {
          // console.log(data);
          tabelku.row.add(data.data).draw(false).nodes().to$().addClass('success');
          tabelku.page('first').draw(false);
          setTimeout(function(){
            $('#dataScanned tr.success').removeClass('success');
          }, 3000);
          countScanned(id_master_setoran);
        }
      },
      error : function(jqXHR,errorThrown,textStatus)
      {
        alert('error getting data from server!'+textStatus);
      }
    });


    // tabelku.row.add([1,2,3,4,5,6,7,8,dd]).draw(false).nodes().to$().addClass('success');
    // // table.order([1, 'asc']).draw();
    // tabelku.page('last').draw(false);
    // setTimeout(function(){
    //     $('#dataScanned tr.success').removeClass('success');
    // }, 3000);
  }

</script>

</body>

</html>
