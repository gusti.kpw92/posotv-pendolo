<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-3">
      <h2><?php echo ucwords(str_replace('_',' ',$active)); ?></h2>
    </div>
    <div class="col-md-6">
    </div>
    <div class="col-md-3">
      <span>Camera Actived</span>
      <span id="listCamera"></span>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
      <div class="wrapper wrapper-content animated fadeInRight">
        <!-- <div class="row"> -->
          <div class="col-lg-6">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-lg-12">
                    <form class="" action="#">
                      <input type="number" name="id_master_setoran" id="id_master_setoran" value="<?=$detail_setoran->id_master_setoran?>" hidden>
                      <input type="number" name="id_kolektor" id="id_kolektor" value="<?=$detail_setoran->id_kolektor?>" hidden>
                    </form>
                    <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                              <span class="pull-right">
                                  <h3><strong class='text-success'><?=$detail_setoran->kolektor ?></strong></h3>
                              </span>
                              <strong>Kolektor</strong>
                            </li>
                            <li class="list-group-item">
                              <span class="pull-right">
                                  <strong class='text-success'><?=date('d M Y',strtotime($detail_setoran->tgl_setoran)) ?></strong>
                              </span>
                              <strong>Tanggal Setoran</strong>
                            </li>
                            <li class="list-group-item">
                              <span class="pull-right">
                                <?=($detail_setoran->acc == 'N') ? "<strong class='text-danger'>$detail_setoran->acc | ".number_format($detail_setoran->total_setoran,0,",",".")."</strong>" : "<strong class='text-success'>$detail_setoran->acc | ".number_format($detail_setoran->total_setoran,0,",",".")."</strong>"; ?>
                              </span>
                              <strong>ACC | Total Setoran</strong>
                            </li>
                            <li class="list-group-item">
                              <span class="pull-right">
                                <?=number_format($detail_setoran->komisi,0,",",".") ?>
                              </span>
                              <strong>Komisi</strong>
                            </li>
                            <li class="list-group-item">
                              <span class="pull-right">
                                <?=$detail_setoran->keterangan ?>
                              </span>
                              <strong>Keterangan</strong>
                            </li>
                        </ul>
                  </div>

                </div>
              </div>
            </div>
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>Pindai Kwitansi <small><?php echo ucwords(str_replace('_',' ',$active)); ?></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-lg-4 b-r">
                    <video class="vidscan img-rounded" id="preview"></video>
                  </div>
                  <div class="col-lg-8">
                    <span class="help-block m-b-none text-warning font-italic">Masukan No. Invoice jika scanner tidak bekerja!</span>
                    <div class="input-group"><input type="text" name="kode_invoice" placeholder="No. Invoice" class="form-control" onkeyup="setUpper()">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" onclick="getDetail('kode','inputmanual')"><span class="fa fa-search"></span> Go!</button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-lg-6">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>Keterangan</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <table id="table" class="table table-hover table-condensed">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th class='text-right'>Iuran</th>
                        <th class='text-right'>Jumlah</th>
                        <th class='text-right'>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody id="kwitansiCount">
                      <tr class="noData">
                        <td colspan="4" class="text-center text-danger"><h3>Tidak ada data!</h3></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>Tabel <?php echo ucwords(str_replace('_',' ',$active)); ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
              </div>
              <div class="ibox-content">
                <div class="table-responsive">
                  <form id="formScanned" action="#" method="post">
                    <table class="table table-hover table-condensed" id="table2">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>No Invoice</th>
                            <th>Kode Pelanggan</th>
                            <th>Nama Lengkap</th>
                            <th>Wilayah</th>
                            <th>Bulan</th>
                            <th>Status</th>
                            <th>Tarif</th>
                            <th class="text-center">
                              <a href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="deleteAllBy(<?=$detail_setoran->id_master_setoran?>)"><i class="fa fa-trash"></i> Del All</button>
                            </th>
                        </tr>
                      </thead>
                      <tbody id="dataScanned">

                      </tbody>
                    </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <form id="formKet" action="#" class="form-horizontal">
                      <input type="text" name="md_kode_pelanggan" hidden>
                      <input type="text" name="md_kode_invoice" hidden>
                      <div class="col-lg-12">
                        <div class="form-group"><label class="col-lg-2 control-label ">Kode Pelanggan</label>
                            <div class="col-lg-10"> <h3><span class="text-primary" id="md_kd_pelanggan"></span></h3>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Keterangan</label>
                            <div class="col-lg-10">
                              <textarea name="md_keterangan" placeholder="Keterangan" class="form-control"></textarea>
                              <span class="text-info">
                                Keterangan ini akan ditampilkan pada kwitansi sebagai catatan untuk kolektor/pelanggan/admin. Contoh : Lunas s/d Jan 2019, TV Rusak, dll.
                              </span>
                            </div>
                        </div>
                      </div>
                  </form>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnSave" onclick="saveKeterangan()">Save changes</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
