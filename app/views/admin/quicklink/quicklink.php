<div class="row">
    <div class="col-lg-12">
      <div class="wrapper wrapper-content animated fadeInRight">
        <div class="p-w-md m-t-sm">
            <div class="row">

                <div class="col-sm-8">
                  <div class="m-l-n-xs">
                    <img src="<?=base_url('assets/img/poso.tv.xs.png')?>" class="img-rounded" alt="" width=""><br>
                  </div>

                    <small>
                      PT. POSO MEDIA VISION
                    </small>
                    <br><br>
                    <!-- <div class="border-bottom"></div> -->
                    <!-- <div class="row"></div> -->
                    <!-- <div class="border-bottom"></div> -->
                </div>

                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">

                </div>

            </div>

            <div class="row">
              <div class="col-lg-9">
                <div>
                  <h3>Master Data</h3>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                              <a href="<?=site_url('dashboard/profil_perusahaan')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-address-card-o"></span>
                                </button>
                                Profil Perusahaan
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/wilayah')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-map"></span>
                                </button>
                                Wilayah
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/pelanggan')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-users"></span>
                                </button>
                                Pelanggan
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/status')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-male"></span>
                                </button>
                                Status Pelanggan
                              </a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                              <a href="<?=site_url('dashboard/tarif')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-dollar"></span>
                                </button>
                                Tarif Berlangganan
                              </a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>

                        <tr>
                          <td colspan="4"><strong>Karyawan</strong></td>
                        </tr>

                        <tr>
                            <td>
                              <a href="<?=site_url('dashboard/bagian')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-sitemap"></span>
                                </button>
                                Bagian
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/jabatan')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-briefcase"></span>
                                </button>
                                Jabatan
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/karyawan')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-user-o"></span>
                                </button>
                                Karyawan
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/kolektor')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-user-secret"></span>
                                </button>
                                Kolektor
                              </a>
                            </td>
                        </tr>

                        <tr>
                          <td colspan="4"><strong>Transaksi</strong></td>
                        </tr>

                        <tr>
                            <td>
                              <a href="<?=site_url('dashboard/kwitansi')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-file-text"></span>
                                </button>
                                Buat Kwitansi
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/master_setoran')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-th-list"></span>
                                </button>
                                Master Setoran
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/tunggakan')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-list-alt"></span>
                                </button>
                                Cek Tagihan
                              </a>
                            </td>
                            <td>
                              <a href="<?=site_url('dashboard/setoran_operator')?>">
                                <button type="button" class="btn btn-default m-r-sm">
                                  <span class="fa fa-money"></span>
                                </button>
                                Bayar Tagihan
                              </a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>

              </div>

              <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                  <!-- <a onclick="<?=site_url('pengaduan/list_view')?>"> -->
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-newspaper-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Display Keluhan </span>
                            <h2 class="font-bold">24</h2>
                        </div>
                    </div>
                  <!-- </a> -->
                </div>
              </div>

            </div>


            <!-- <div class="row">
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content">

                        </div>
                    </div>
                </div>
            </div> -->

        </div>

      </div>
