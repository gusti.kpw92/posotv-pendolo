<?php

class PDF extends FPDF
{
function Potong()
{
  $this->setY(82.5);
  $this->setFont('Arial','',5);
  $this->setFillColor(255,255,255);
    for ($i=0; $i < 215; $i++) {
      $this->cell(1,5,'-',0,0,'C',0);
    }
}

public function Terbilang($satuan) {
  $huruf = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas' );
  if ($satuan < 12) {
    return ' '.$huruf[$satuan];
  }
  elseif ($satuan < 20) {
    return ' '.$huruf[$satuan-10].'Belas ';
  }
  elseif ($satuan < 100) {
    return ''.$huruf[$satuan/10].' Puluh '.$huruf[$satuan%10];
  }
  elseif ($satuan < 200) {
    return ' Seratus'. $this->Terbilang($satuan-100);
  }
  elseif ($satuan < 1000) {
    return $this->Terbilang($satuan/100).'Ratus '.$this->Terbilang($satuan % 100).' ';
  }
  elseif ($satuan < 2000) {
    return ' Seribu'. $this->Terbilang($satuan-1000);
  }
  elseif ($satuan < 1000000) {
    return $this->Terbilang($satuan/1000).' Ribu'.$this->Terbilang($satuan%1000);
  }
  elseif ($satuan < 1000000000) {
    return $this->Terbilang($satuan/1000000).'Juta '.$this->Terbilang($satuan % 1000000);
  }
  elseif ($satuan <= 1000000000) {
    echo 'Maaf, tidak dapat diproses karena jumlah uang terlalu besar';
  }
}

function Kop()
{
  $a = 0; $y=4; $geserx = 44;
  for ($i=1; $i <=12 ; $i++) {
    $a++;
    if ($i % 2 == 0) {
      $this->setY($y);
      $dx = 163+$geserx;
      $gxl = 15+158+$geserx;
      $gxr = 130+158+$geserx;

      $xkanan = 50+158+$geserx;
      $xkanan1 = 105+158+$geserx;
      $xkanan2 = 155+158+$geserx;
      $xkiri = 55+157+$geserx;
      $xkiri2 = 25+157+$geserx;
    } else {
      $dx = 5+$geserx; //default x
      $gxl = 15+$geserx; // default gambar x kiri
      $gxr = 130+$geserx; // default gambar x kanan

      $xkanan = 50+$geserx;
      $xkanan1 = 105+$geserx;
      $xkanan2 = 155+$geserx;
      $xkiri = 55+$geserx;
      $xkiri2 = 25+$geserx;
    }

    if ($a==1) {
      $this->setY(4);
      $yg1 = 27;
      $yg2 = 9;
    }
    if ($a==3) {
      $y = 63;
      $this->setY($y);
      $yg1 = 27+59;
      $yg2 = 9+59;
    }
    if ($a==5) {
      $y = 122;
      $this->setY($y);
      $yg1 = 27+118;
      $yg2 = 9+118;
    }
    if ($a==6) {
      $y = 122;
      $this->setY($y);
    }

    $qr = 'assets/tempQr/img/download.png';
    // $yg1 = 27;
    // $yg2 = 9;
    $this->setX($dx);  $this->Image($qr,$gxl,$yg1,18,18); // Tengah kiri
    $this->setX($dx);  $this->Image($qr,$gxr,$yg2,18,18); // Pojok kanan atas

    $this->setFont('Arial','B',9);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,3,'PT. POSO MEDIA VISION',0,0,'L',0);
    $this->setX($xkanan); $this->cell($xkanan,3,'PT. POSO MEDIA VISION',0,0,'L',0); // Robekan Bagian Kanan
    $this->setFont('Arial','I',9);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan); $this->cell(110,3,'BUKTI PEMBAYARAN       |',0,0,'R',0);

    $this->Ln();
    $this->setFont('Arial','I',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,3,'Lembaga Penyiaran Berlanggan',0,0,'L',0);
    $this->setX($xkanan); $this->cell($xkanan,3,'Lembaga Penyiaran Berlanggan',0,0,'L',0); // Robekan Bagian Kanan

    $this->Ln(4);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,2,'Kode Pelanggan',0,0,'L',0);
    $this->setX($xkiri2); $this->cell($xkiri,2,'Tarif Iuran',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'Kode Pelanggan',0,0,'L',0); // Robekan Bagian Kanan
    $this->setX($xkanan1-25);  $this->cell(45,2,'Bulan Tagihan',0,0,'L',0); // Robekan Bagian Kanan1
    $this->setX($xkanan2-50);  $this->cell(45,2,'Tarif Iuran',0,1,'L',0); // Robekan Bagian Kanan2

    $this->setFont('Arial','B',8);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,3,'PDL00'.$i,0,0,'L',0);
    $this->setX($xkiri2);  $this->cell($xkiri,3,'Rp. 30.000,-',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,3,'PDL00'.$i,0,0,'L',0); // Robekan Bagian Kanan
    $this->setX($xkanan1-25);  $this->cell(45,3,'September 2018',0,0,'L',0); // Robekan Bagian Kanan1
    $this->setFont('Arial','B',8);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan2-50);  $this->cell(45,3,'Rp. 30.000,-',0,0,'L',0); // Robekan Bagian Kanan2
    $this->Ln(4);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,2,'Nama Pelanggan',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'Nama Pelanggan',0,0,'L',0); // Robekan Bagian Kanan
    $this->setX($xkanan2-50);  $this->cell(45,2,'CS/Teknisi',0,1,'L',0); // Robekan Bagian Kanan2

    $this->setFont('Arial','B',8);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,3,'Gusti Ketut P. Wijaya',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,3,'Gusti Ketut P. Wijaya',0,0,'L',0); // Robekan Bagian Kanan

    $this->setFont('Arial','B',8);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan2-50);  $this->cell(45,3,'0852 9847 0228',0,0,'L',0); // Robekan Bagian Kanan

    $this->Ln(4);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,2,'Alamat',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'Alamat',0,1,'L',0); // Robekan Bagian Kanan


    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,2,'LAPANGAN PANTANDE',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'LAPANGAN PANTANDE',0,0,'L',0); // Robekan Bagian Kanan

    $this->Ln(3);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell($xkiri,2,'Jumlah Terbilang :',0,0,'L',0);
    $this->setX($xkanan+79);  $this->cell(45,2,'No. Invoice',0,1,'L',0); // Robekan Bagian Kanan

    $this->setFont('Courier','BI',8);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell($xkiri,3,preg_replace('/\s\s+/', ' ', $this->Terbilang(30000).'Rupiah'),0,0,'L',0);
    $this->setFont('Courier','B',7);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan+79);  $this->cell(45,3,'PDL1811319458',0,0,'L',0); // Robekan Bagian Kanan

    $this->Ln(4);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell($xkiri,2,'Keterangan :',0,0,'L',0);
    $this->setX($xkanan+57);  $this->cell(45,2,'Pendolo, 02/11/2018',0,1,'R',0); // Robekan Bagian Kanan

    $this->setFont('Courier','BI',8);
    $this->setFillColor(255,255,255);
    // $this->setX($xkanan);  $this->cell($xkiri,3,$this->Terbilang(30000).'Rupiah',0,0,'L',0);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan+57);  $this->cell(45,2,'Kolektor',0,1,'R',0); // Robekan Bagian Kanan

    $this->Ln(7);
    $this->setFont('Courier','B',7);
    $this->setFillColor(255,255,255);
    $this->setX($dx+8);  $this->cell($xkiri,2,'PDL1811319458',0,0,'L',0);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell(45,2,'Penagihan dimulai tanggal 2 s/d 15 setiap bulannya.',0,0,'L',0); // Robekan Bagian Kanan1
    $this->setFont('Arial','B',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan+57);  $this->cell(45,2,'[ PAPA ANGEL ]',0,1,'R',0); // Robekan Bagian Kanan
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell(45,2,'* Sertakan kwitansi lama untuk pembayaran iuran selanjutnya.',0,1,'L',0); // Robekan Bagian Kanan1
    $this->setX($dx);  $this->cell($xkiri,2,'Bulan Tagihan',0,0,'L',0);
    $this->setX($xkiri2); $this->cell($xkiri,2,'Kolektor',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'* Menunggak 2 (dua) bulan akan dilakukan pemutusan sementara',0,1,'L',0); // Robekan Bagian Kanan1

    $this->setFont('Arial','B',6);
    $this->setFillColor(255,255,255);
    $this->setX($dx);  $this->cell($xkiri,2,'September 2018',0,0,'L',0);
    $this->setX($xkiri2);  $this->cell($xkiri,2,'Papa Angel',0,0,'L',0);
    $this->setFont('Arial','',6);
    $this->setFillColor(255,255,255);
    $this->setX($xkanan);  $this->cell(45,2,'  dan disambung kembali setelah melunasi tunggakan.',0,1,'L',0); // Robekan Bagian Kanan1
    $this->setX($dx);  $this->cell($xkiri,2,'Keterangan :',0,0,'L',0);
    $this->setX($xkanan);  $this->cell(45,2,'* SMS Keluhan Anda dengan menyertakan NAMA, KODE PELANGGAN dan ALAMAT.',0,1,'L',0); // Robekan Bagian Kanan1

    $this->Ln(2);
    $this->setX($dx);  $this->cell($xkiri,2,'_________________________________________________________________________________________________________________________________ |',0,0,'L',0);
    $this->Ln(7);

    if ($a==6) {
  		$this->AddPage();
      $a = 0; $y = 4;
  	}
  }
}

}

$pageSize = array(220,360);
$pdf = new PDF('L','mm',$pageSize);
$pdf->setTopMargin(8);
$pdf->SetCreator('POSO TV App');
$pdf->SetAuthor('Rahtut Aza');
$pdf->SetAutoPageBreak(true,3);
$pdf->AddPage();
$pdf->Kop();
$pdf->Output('kwitansi.pdf','I');
